#!/bin/sh
# Copyright © 2022 Simon McVittie
# SPDX-License-Identifier: FSFAP

echo "TAP version 13"

if ! command -v reuse >/dev/null; then
    echo "1..0 # SKIP reuse not found"
    exit 0
fi

export LC_ALL=C.UTF-8
echo "1..1"

if [ -n "${GDP_SRCDIR+set}" ]; then
    cd "$GDP_SRCDIR"
fi

if reuse lint >&2; then
    echo "ok 1 - REUSE-compliant"
elif [ -n "${LINT_WARNINGS_ARE_ERRORS-}" ]; then
    echo "not ok 1 - not REUSE-compliant"
    exit 1
else
    echo "not ok 1 # TODO not REUSE-compliant"
fi
