# Copyright 2023 Alexandre Detiste
# SPDX-License-Identifier: FSFAP

# this goes in /usr/lib/python3/dist-packages/omg/__init__.pyi
# + touch /usr/lib/python3/dist-packages/omg/py.typed

from PIL.Image import Image

class Graphic:

    def to_Image(self) -> Image: ...

class WAD:

    graphics: dict[str, Graphic]

    def __init__(self, from_file:str): ...
