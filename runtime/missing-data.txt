Required data files are missing.

Please use game-data-packager to build and install the data packages
for ${name}.

Typical use of game-data-packager:

game-data-packager -i ${game} [PATH [PATH...]]

    Install game data for ${name}, searching PATH for data files

game-data-packager -d ~/Downloads ${game} [PATH [PATH...]]

    Generate game data packages in ~/Downloads to install later

For more details please run "game-data-packager ${game} --help"
or see the game-data-packager(6) manual page.
