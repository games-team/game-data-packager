#!/bin/sh
# Copyright 2022 Simon McVittie
# SPDX-License-Identifier: FSFAP

set -eu

source_file="$1"
shift
mode="$1"
shift

for destination in "$@"; do
    case "$destination" in
        (/*)
            install -d "$DESTDIR$(dirname "$destination")"
            install -m"$mode" "$source_file" "$DESTDIR$destination"
            ;;
        (*)
            install -d "$MESON_INSTALL_DESTDIR_PREFIX/$(dirname "$destination")"
            install -m"$mode" "$source_file" "$MESON_INSTALL_DESTDIR_PREFIX/$destination"
            ;;
    esac
done

# vim:set sw=4 sts=4 et:
