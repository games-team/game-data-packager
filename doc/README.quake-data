Installing Quake data
=====================

Please use game-data-packager to build and install the quake-registered
or quake-shareware package. Only one version at a time can be installed.

For quake-registered you will need a Quake CD-ROM, or at least id1/pak1.pak
from a Quake installation. game-data-packager can download all other
necessary files. Typical uses:

    game-data-packager -d ~/Downloads quake /path/to/search
        Save the generated package to ~/Downloads to install later

    game-data-packager --install quake /path/to/search
        Install the generated package, do not save it for later

game-data-packager will automatically search various common installation
paths. If you are installing from a CD-ROM or a non-standard location,
add the installation path or CD-ROM mount point to the command line.

If you have other game files available locally, you can reduce downloading
by adding the path to id1/pak0.pak version 1.06, quake106.zip,
setup_quake_the_offering_2.0.0.6.exe or resource.1 to the command line.

For more details please see the output of "game-data-packager quake3 --help"
or the game-data-packager(6) man page.

Shareware version
=================

For the shareware version, run something like this:

    game-data-packager -d ~/Downloads quake --demo

and add the path to quake106.zip to the command line if you already have it.

Music
=====

For the music, either provide a directory containing pre-ripped
audio named like id1/music/track02.ogg, hipnotic/music/track02.ogg or
rogue/music/track02.ogg, or use syntax like

    game-data-packager -d ~/Downloads quake --package=quake-music /dev/cdrom

to rip and encode CD audio tracks.

You might be able to download suitable audio files here:
https://steamcommunity.com/sharedfiles/filedetails/?id=119489135

Expansions
==========

If you have the expansions Scourge of Armagon (hipnotic), Dissolution
of Eternity (rogue) or Dimensions of the Past (dopa), add their
installation directories or CD-ROM mount points to the command line.
